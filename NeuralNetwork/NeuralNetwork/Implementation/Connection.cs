﻿using NeuralNetwork.Interfaces;

namespace NeuralNetwork.Implementation
{
    public class Connection : IConnection
    {
        public INeuron Neuron { get; set; }

        public double Weight { get; set; }
    }
}
