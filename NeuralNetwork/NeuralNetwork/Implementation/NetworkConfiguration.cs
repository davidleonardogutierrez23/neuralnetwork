﻿using System;
using System.Collections.Generic;

namespace NeuralNetwork.Implementation
{
    public class NetworkConfiguration
    {
        public NetworkConfiguration(List<int> layers)
        {
            this.Layers = layers;
        }

        /// <summary>
        /// Function to execute in the neuron for the inputs
        /// </summary>
        public Func<IList<double>, double> InputFunction { get; set; }

        /// <summary>
        /// Function to execute in the neuron for the activation
        /// </summary>
        public Func<double, double> ActivationFunction { get; set; }

        /// <summary>
        /// Function to execute in the neuron for the output
        /// </summary>
        public Func<double, double> OutputFunction { get; set; }

        /// <summary>
        /// Layers
        /// </summary>
        public List<int> Layers
        {
            get;
            set;
        }

        /// <summary>
        /// Clone the config
        /// </summary>
        /// <returns></returns>
        public NetworkConfiguration Clone()
        {
            var layers = new List<int>();

            foreach (var layer in this.Layers)
            {
                layers.Add(layer);
            }

            return new NetworkConfiguration(layers);
        }
    }
}
