﻿using DependencyInjection;
using NeuralNetwork.Functions;
using NeuralNetwork.Interfaces;
using System.Collections.Generic;
using Utilities;

namespace NeuralNetwork.Implementation
{
    public static class NetworkBuilder
    {
        public static INetwork Build(NetworkConfiguration config)
        {
            var network = Dependency.Solve<INetwork>();

            config.Layers.Reverse();

            ILayer currentLayer;
            ILayer lastLayer = null;

            foreach (var layerConfig in config.Layers)
            {
                currentLayer = Dependency.Solve<ILayer>();

                currentLayer.NextLayer = lastLayer;
                currentLayer.Bias = Dependency.Solve<INeuron>();
                currentLayer.Bias.InputFunction = config.InputFunction;
                currentLayer.Bias.ActivationFunction = config.ActivationFunction;
                currentLayer.Bias.OutputFunction = config.OutputFunction;

                INeuron neuron;

                for (int i = 0; i < layerConfig; i++)
                {
                    neuron = Dependency.Solve<INeuron>();

                    neuron.InputFunction = config.InputFunction;
                    neuron.ActivationFunction = config.ActivationFunction;
                    neuron.OutputFunction = config.OutputFunction;

                    if (lastLayer != null)
                    {
                        IConnection connection;
                        
                        foreach (var conNeuron in lastLayer.Neurons)
                        {
                            connection = Dependency.Solve<IConnection>();
                            connection.Neuron = conNeuron;
                            connection.Weight = RandomSingleton.GetRandomInstance().Next(-2, 2);
                            neuron.Connections.Add(connection);    
                        }
                    }
                    
                    currentLayer.Neurons.Add(neuron);
                }

                if (lastLayer != null)
                {
                    IConnection connectionBias;

                    foreach (var conNeuron in lastLayer.Neurons)
                    {
                        connectionBias = Dependency.Solve<IConnection>();
                        connectionBias.Neuron = conNeuron;
                        connectionBias.Weight = RandomSingleton.GetRandomInstance().Next(-2, 2);
                        currentLayer.Bias.Connections.Add(connectionBias);
                    }
                }

                network.Layers.Insert(0, currentLayer);

                lastLayer = currentLayer;
            }

            network.Configuration = config;

            return network;
        }
    }
}
