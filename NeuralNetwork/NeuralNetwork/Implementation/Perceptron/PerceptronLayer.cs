﻿using NeuralNetwork.Interfaces;
using System.Collections.Generic;

namespace NeuralNetwork.Implementation.Perceptron
{
    public class PerceptronLayer : ILayer
    {
        public const double BiasInput = 1;

        /// <summary>
        /// Bias neuron
        /// </summary>
        public INeuron Bias{ get; set; }

        /// <summary>
        /// List of the neurons in the layer
        /// </summary>
        public IList<INeuron> Neurons { get; set; }

        public ILayer NextLayer{ get; set; }

        /// <summary>
        /// Send to each neuron the resut of a neuron exec
        /// </summary>
        /// <param name="neuronResult"></param>
        public void ExecuteLayer()
        {
            foreach (var neuron in this.Neurons)
            {
                neuron.ExecuteNeuron();
            }

            Bias.Inputs.Add(BiasInput);
            this.Bias.ExecuteNeuron();
        }

        /// <summary>
        /// Send to each neuron the resut of a neuron exec
        /// </summary>
        /// <param name="neuronResult"></param>
        public void Learn(double answer)
        {
            
        }

        /// <summary>
        /// Public Constructor
        /// </summary>
        public PerceptronLayer()
        {
            this.Neurons = new List<INeuron>();
        }
    }
}
