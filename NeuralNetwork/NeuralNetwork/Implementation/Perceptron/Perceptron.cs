﻿using NeuralNetwork.Interfaces;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetwork.Implementation.Perceptron
{
    public class Perceptron : INetwork
    {
        public IList<ILayer> Layers { get; set; }

        public NetworkConfiguration Configuration { get; set; }

        /// <summary>
        /// Result of execution
        /// </summary>
        public IList<double> Result { get; set; }
        
        /// <summary>
        /// Execute the network
        /// </summary>
        /// <param name="feed"></param>
        /// <returns></returns>
        public void ExecuteNetwork(List<double> input)
        {
            this.CleanInputs();

            foreach (var neuron in this.Layers[0].Neurons)
            {
                neuron.Inputs = input;
            }

            foreach (var layer in this.Layers)
            {
                layer.ExecuteLayer();
            }

            foreach (var neuron in this.Layers[this.Layers.Count - 1].Neurons)
            {
                this.Result.Add(neuron.Output);
            }
        }

        /// <summary>
        /// Training the network
        /// </summary>
        /// <param name="feed"></param>
        /// <returns></returns>
        public void Learn()
        {
            List<double> input = new List<double>() { 1, 8, 7, 5, 9, 4, 4, 5, 6, 33, 3, 4, 4 };
            
        }

        /// <summary>
        /// Get the network execution result as strig
        /// </summary>
        /// <returns></returns>
        public string GetResult()
        {
            StringBuilder text = new StringBuilder();

            text.Append("Result:");

            foreach (var result in this.Result)
            {
                text.Append(" {" + result + "}");

            }

            return text.ToString();
        }

        /// <summary>
        /// Converts the network to a string for debug propuses
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder text = new StringBuilder();

            foreach (var layer in this.Layers)
            {
                text.Append(" --[ ");

                if (layer.Bias != null)
                {
                    text.Append(" B ");
                }

                foreach (var neuron in layer.Neurons)
                {
                    text.Append(" N ");
                }
                text.Append($"   ({layer.Neurons.Count}) ] \n");
            }

            return text.ToString();
        }

        /// <summary>
        /// Clear the neurons inputs
        /// </summary>
        public void CleanInputs()
        {
            this.Result.Clear();

            foreach (var layer in this.Layers)
            {
                foreach (var neuron in layer.Neurons)
                {
                    neuron.Inputs.Clear();
                }

                layer.Bias.Inputs.Clear();
            }
        }

        /// <summary>
        /// Class constructor
        /// </summary>
        public Perceptron()
        {
            this.Layers = new List<ILayer>();
            this.Result = new List<double>();
        }
    }
}
