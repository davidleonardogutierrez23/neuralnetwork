﻿using System;
using NeuralNetwork.Interfaces;
using System.Collections.Generic;

namespace NeuralNetwork.Implementation
{
    public class Neuron : INeuron
    {
        public IList<IConnection> Connections { get; set; }

        public IList<double> Inputs { get; set; }

        public double Output { get; set; }

        public Func<IList<double>, double> InputFunction { get; set; }

        public Func<double, double> ActivationFunction { get; set; }

        public Func<double, double> OutputFunction { get; set; }
        
        /// <summary>
        /// execuet the neuron
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public void ExecuteNeuron()
        {
            double globalInput = this.InputFunction(this.Inputs);
            double activation = this.ActivationFunction(globalInput);
            this.Output = this.OutputFunction(activation);

            foreach (var connection in this.Connections)
            {
                connection.Neuron.Inputs.Add(this.Output);
            }
        }

        /// <summary>
        /// function to trainig the network
        /// </summary>
        /// <param name="values"></param>
        /// <param name="answer"></param>
        /// <returns></returns>
        public void Learn(IList<double> input, double answer)
        {
            
        }

        /// <summary>
        /// Public Constructor
        /// </summary>
        public Neuron()
        {
            this.Connections = new List<IConnection>();
            this.Inputs = new List<double>();
        }
    }
}
