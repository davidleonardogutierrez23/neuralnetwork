﻿using NeuralNetwork.Interfaces;
using System.Collections.Generic;

namespace NeuralNetwork.Interfaces
{
    public interface ILayer
    {
        IList<INeuron> Neurons { get; set; }

        ILayer NextLayer { get; set; }

        INeuron Bias { get; set; }

        //Feed each neuron in the layer with the feed
        void ExecuteLayer();

        void Learn(double answer);
    }
}
