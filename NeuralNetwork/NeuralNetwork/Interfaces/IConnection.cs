﻿namespace NeuralNetwork.Interfaces
{
    public interface IConnection
    {
        double Weight { get; set; }

        INeuron Neuron { get; set; }
    }
}
