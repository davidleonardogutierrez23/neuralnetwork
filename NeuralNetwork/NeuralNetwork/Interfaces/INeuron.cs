﻿using System;
using System.Collections.Generic;

namespace NeuralNetwork.Interfaces
{
    public interface INeuron
    {
        IList<IConnection> Connections { get; set; }

        IList<double> Inputs { get; set; }

        double Output { get; set; }

        Func<IList<double>, double> InputFunction { get; set; }

        Func<double, double> ActivationFunction { get; set; }

        Func<double, double> OutputFunction { get; set; }

        void ExecuteNeuron();

        void Learn(IList<double> input, double answer);
    }
}
