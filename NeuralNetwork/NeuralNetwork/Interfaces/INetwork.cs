﻿using NeuralNetwork.Implementation;
using System.Collections.Generic;

namespace NeuralNetwork.Interfaces
{
    public interface INetwork
    {
        IList<ILayer> Layers { get; set; }

        IList<double> Result { get; set; }

        NetworkConfiguration Configuration { get; set; }

        void ExecuteNetwork(List<double> input);

        void Learn();

        string GetResult();

        void CleanInputs();
    }
}
