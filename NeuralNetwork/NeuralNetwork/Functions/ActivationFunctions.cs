﻿using System;

namespace NeuralNetwork.Functions
{
    public static class ActivationFunctions
    {
        public static double LinealFunction(double x)
        {
            int threshold = 0;
            return x - threshold;
        }

        public static double SigmoidFunction(double x)
        {
            return 1 / (1 + (Math.Exp(-x)));
        }

        public static double HyperbolicFunction(double x)
        {
            return (Math.Exp(2 * x) - 1)/(Math.Exp(2* x) + 1);
        }
    }
}
