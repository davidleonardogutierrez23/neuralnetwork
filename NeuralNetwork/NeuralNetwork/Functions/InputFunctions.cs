﻿using System.Collections.Generic;
using System.Linq;

namespace NeuralNetwork.Functions
{
    public static class InputFunctions
    {
        public static double SummationHeavyInputs(IList<double> values)
        {
            double result = 0;

            for (int i = 0; i < values.Count(); i++)
            {
                result += values[i];
            }

            return result;
        }

        public static double ProductoryHeavyInputs(IList<double> values)
        {
            double result = 0;

            for (int i = 0; i < values.Count(); i++)
            {
                result *= values[i];
            }

            return result;
        }

        public static double MaxHeavyInputs(IList<double> values)
        {
            double max = values.Max();
            
            return max;
        }
    }
}
