﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjection
{
    public static class Dependency
    {
        /// <summary>
        /// Get an instance that implements the given interface
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tipe"></param>
        /// <returns></returns>
        public static T Solve<T>()
        {
            var type = typeof(T);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p));

            return (T)Activator.CreateInstance(types.Where(t => t.BaseType != null).FirstOrDefault());
        }
    }
}
