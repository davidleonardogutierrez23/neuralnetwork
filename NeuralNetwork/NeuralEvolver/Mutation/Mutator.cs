﻿using NeuralNetwork.Implementation;
using NeuralNetwork.Interfaces;
using System.Collections.Generic;
using Utilities;

namespace NeuralEvolver.Mutation
{
    public class Mutator
    {
        private const int maxChlds = 20;

        private const int minNeuron = 3;
        private const int maxNeuron = 20;

        private const int minLayer = 3;
        private const int maxLayer = 20;

        /// <summary>
        /// Mutate the given network
        /// </summary>
        /// <param name="network"></param>
        /// <returns></returns>
        public static List<INetwork> Mutate(INetwork network)
        {
            var children = new List<INetwork>();

            //Add a new child with the same parent configutarion
            children.Add(NetworkBuilder.Build(network.Configuration.Clone()));

            var rnd = RandomSingleton.GetRandomInstance();
            var numChld = rnd.Next(0, maxChlds);

            for (int i = 0; i < numChld; i++)
            {
                NetworkConfiguration newConfig = null;
                var layers = new List<int>();

                var mutLayers = rnd.Next(0, 2);
                if (mutLayers == 1)
                {
                    var nLayers = rnd.Next(minLayer, maxLayer);

                    for (int c = 0; c < nLayers; c++)
                    {
                        layers.Add(rnd.Next(minNeuron, maxNeuron));
                    }
                    
                    newConfig = new NetworkConfiguration(layers);
                    children.Add(NetworkBuilder.Build(newConfig));
                    continue;
                }

                var mutNeurons = rnd.Next(0, 1);
                if (mutLayers == 1)
                {
                    var nLayers = rnd.Next(minLayer, maxLayer);

                    for (int c = 0; c < network.Configuration.Layers.Count; c++)
                    {
                        layers.Add(rnd.Next(minNeuron, maxNeuron));
                    }

                    newConfig = new NetworkConfiguration(layers);
                    children.Add(NetworkBuilder.Build(newConfig));
                    continue;
                }

                children.Add(NetworkBuilder.Build(network.Configuration.Clone()));
            }
            
            return children;
        }
    }
}
