﻿using DependencyInjection;
using NeuralEvolver.interfaces;
using NeuralEvolver.Mutation;
using NeuralEvolver.Tree;
using NeuralNetwork.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralEvolver.Manager
{
    public class NetworkManager
    {
        public Node Root;

        private const int MaxTreeLevel = 3;

        public void CreateTree(NetworkConfiguration rootConf)
        {
            var root = NetworkBuilder.Build(rootConf);
            var chld = Mutator.Mutate(root);
            var selector = Dependency.Solve<ISelector>();

            this.Root = new Node(root, chld, 0, MaxTreeLevel, selector);

            int Count = this.Root.Count();
        }
    }
}
