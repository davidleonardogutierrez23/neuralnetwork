﻿using NeuralEvolver.interfaces;
using NeuralEvolver.Mutation;
using NeuralNetwork.Implementation;
using NeuralNetwork.Interfaces;
using System.Collections.Generic;

namespace NeuralEvolver.Tree
{
    public class Node
    {
        public INetwork Network { get; }
        public List<Node> Childrens { get; }
        private ISelector selector;

        public Node(INetwork parent, List<INetwork> children, int level, int maxLevel, ISelector selector)
        {
            this.Network = parent;

            if (level < maxLevel)
            {
                this.Childrens = new List<Node>();

                foreach (var child in children)
                {
                    //Put here the seletor validation for the child
                    if (this.selector.Test(child))
                    {
                        var chl = Mutator.Mutate(child);

                        this.Childrens.Add(new Node(child, chl, level + 1, maxLevel, this.selector));
                    }
                }
            }
            else
            {
                this.Childrens = null;
            }
        }

        /// <summary>
        /// Count the member in the tree
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            int count = 0;

            if (this.Childrens == null)
            {
                return count;
            }

            foreach (var node in this.Childrens)
            {
                count += node.Count();
            }
            return count + this.Childrens.Count;
        }
    }
}
