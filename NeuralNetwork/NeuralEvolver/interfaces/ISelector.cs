﻿using NeuralNetwork.Interfaces;

namespace NeuralEvolver.interfaces
{
    public interface ISelector
    {
        bool Test(INetwork network);
    }
}
