﻿using System;

namespace Utilities
{
    public class RandomSingleton
    {
        private static Random rnd;

        private RandomSingleton()
        {

        }

        public static Random GetRandomInstance()
        {
            if (rnd == null)
            {
                rnd = new Random();
            }
            return rnd;
        }
    }
}
