﻿using NeuralEvolver.Manager;
using NeuralNetwork.Functions;
using NeuralNetwork.Implementation;
using NeuralNetwork.Interfaces;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace NeuralInterface
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            this.config = new NetworkConfiguration(new List<int>() { 3, 5, 9, 10, 8, 7, 6, 5 });
            config.InputFunction = InputFunctions.SummationHeavyInputs;
            config.ActivationFunction = ActivationFunctions.SigmoidFunction;
            config.OutputFunction = OutputFunctions.Binary;

             this.network = NetworkBuilder.Build(config);
        }
        NetworkManager manager = new NetworkManager();

        NetworkConfiguration config;

        INetwork network;


        private void button1_Click(object sender, EventArgs e)
        {
            network.ExecuteNetwork(new List<double>() { 1, 8, 7, 5, 9, 4, 4, 5, 6, 33, 3, 4, 4 });

            this.label1.Text = "";
            this.label1.Text = network.GetResult();
            
            //manager.CreateTree(config);

        }
    }
}
